# Karengera_2021_Wastewater_fingerprinting

Repository of code belonging to the wastewater fingerprinting paper: Fingerprinting toxic potencies in wastewater using gene expression profiling in C. elegans as a bioanalytical tool.